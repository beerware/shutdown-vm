#!/bin/sh

BOXSTATE=$(VBoxManage list runningvms | wc -l);

# Clear cli before start.
clear;

echo "----------------------------------------------------------------------------
   _____ __          __      __                       _    ____  ___
  / ___// /_  __  __/ /_____/ /___ _      ______     | |  / /  |/  /  _____
  \__ \/ __ \/ / / / __/ __  / __ \ | /| / / __ \    | | / / /|_/ /  / ___/
 ___/ / / / / /_/ / /_/ /_/ / /_/ / |/ |/ / / / /    | |/ / /  / /  (__  )
/____/_/ /_/\__,_/\__/\__,_/\____/|__/|__/_/ /_/     |___/_/  /_/  /____/
----------------------------------------------------------------------------";

echo "\033[32m-> Current VMs online: $BOXSTATE\033[33m";

# Count for loops.
count=1
# Show online vm´s.
for BOX in $(VBoxManage list runningvms | awk '{print $1}' | sed 's/"//g'); do
  echo "  \033[34m $count.) $BOX \033[0m";

  arr[count]=$BOX

  count=`expr $count + 1`
done

# Function to shutdown vm by name.
shutdown_vm(){
  $(VBoxManage controlvm $2 acpipowerbutton);
  echo "  \033[31m $1.) $2 Shutdown... \033[0m";
}

# Check if vm´s are online.
if [ $BOXSTATE = 0 ] ;then
  echo "   \033[31m... no VM´s online \033[0m";
else

  # Asking for Shutdown.
  read -e -p "-> Shutdown all VMs (y) or single VM (1,2,...) exit (e)? " answer

  IFS=',' read -ra my_array <<< "$answer"
  answerLen=${#my_array[@]}

  # Kill all vms
  if [ "$answer" != "${answer#[Yy]}" ] ;then
    echo "\033[33m-> Starting shutdown boxes... \033[0m";

    # Count for loops.
    count=1

    # Shutdow all online vm´s.
    for BOX in $(VBoxManage list runningvms | awk '{print $1}' | sed 's/"//g'); do

      # Shutdown VM.
      shutdown_vm $count $BOX

      count=`expr $count + 1`
    done
    echo "\033[33m-> done. \033[0m"

  # Kill single vms.
  elif [ $answerLen -gt 0 ] ;then

    echo "\033[33m-> Starting shutdown box... \033[0m";
    for i in "${my_array[@]}"; do
      entry=${arr[$i]}
      if ! [ -z $entry ] ;then
        # Shutdown VM.
        shutdown_vm $i $entry
      else
        echo "\033[31m-> unexepted value.\033[0m"
      fi
    done
    echo "\033[33m-> done. \033[0m"
  fi
fi
echo "\033[32m-> exit...\033[0m"$'\360\237\215\272'

# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <beer@roboxforge.com> wrote this file. As long as you retain this notice you
# can do whatever you want with this stuff. If we meet some day, and you think
# this stuff is worth it, you can buy me a beer in return Martin Henrichs ;)
# ----------------------------------------------------------------------------



















